import unittest

from data_models import Author


class TestDataModels(unittest.TestCase):
    def test_author_unpacking_from_list(self):
        author1 = Author(['J', 'R', 'R', 'Tolkien'])
        self.assertEqual(['J', 'R', 'R'], author1.names)
        self.assertEqual('Tolkien', author1.surname)
        author2 = Author(['Jan', 'Wadolowski'])
        self.assertEqual(['Jan'], author2.names)
        self.assertEqual('Wadolowski', author2.surname)

    def test_authors_comparing(self):
        author1 = Author(['J', 'R', 'R', 'Tolkien'])
        author2 = Author(['J', 'R', 'R', 'Tolkien'])
        author3 = Author(['J', 'R', 'Tolkien'])
        author4 = Author(['Tolkien', 'J', 'R'])
        self.assertTrue(author1 == author2)
        self.assertTrue(author1 != author3)
        self.assertTrue(author1 != author4)


if __name__ == '__main__':
    unittest.main()
