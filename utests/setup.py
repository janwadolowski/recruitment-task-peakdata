from setuptools import setup

setup(
    name='recruitment-task-peakdata',
    version='1.0.0',
    packages=[''],
    package_dir={'': 'utests'},
    url='',
    license='',
    author='Jan Wadolowski',
    author_email='janwadolowski@gmail.com',
    description='Recruitment task for PeakData: find unique list of authors and institutions based on CSV file'
)
