import os
import unittest

import pandas as pd

from data_parser.data_parser import import_data_from_cvs, _get_series_of_all_authors_occurrences, \
    _convert_author_string_to_list, _syntactic_parser

path_to_cvs = os.path.join(os.getcwd(), 'utests/Resources/publications_min.csv')


@unittest.skipIf(not os.path.exists(path_to_cvs), 'cvs file doesn\'t exist in provided path')
class TestDataParsingAndConverting(unittest.TestCase):
    input_data = import_data_from_cvs(
        cvs_file=path_to_cvs,
        column_headers=['authors'])

    def test__get_list_of_all_authors_occurrences(self):
        data_converted_to_series = _get_series_of_all_authors_occurrences(TestDataParsingAndConverting.input_data)
        self.assertTrue(type(data_converted_to_series) is pd.Series,
                        f"Type of output is not pd.Series. It's {type(data_converted_to_series)}")
        self.assertGreater(data_converted_to_series.size, 0, "The series is empty")
        one_author_str_from_a_series = data_converted_to_series.loc[5]
        self.assertTrue(type(one_author_str_from_a_series) is str,
                        f"Type of output is not str. It's {type(one_author_str_from_a_series)}")

    def test__convert_author_string_to_list(self):
        author1 = 'J P Morgan'
        author1_list = _convert_author_string_to_list(author1)
        author2 = 'Alfred Nobel'
        author2_list = _convert_author_string_to_list(author2)
        author3 = 'Franklin D. Roosevelt'
        author3_list = _convert_author_string_to_list(author3)
        author4 = 'John Ronald Reuel Tolkien'
        author4_list = _convert_author_string_to_list(author4)
        self.assertEqual(3, len(author1_list))
        self.assertEqual(2, len(author2_list))
        self.assertEqual(3, len(author3_list))
        for name in author3_list:
            self.assertTrue('.' not in name, f"'.' in {author3} hasn't been removed")
        self.assertEqual(4, len(author4_list))

    def test__syntactic_parser(self):
        author1 = ['J', 'P', 'Morgan']
        author2 = ['Alfred', 'Nobel']
        author3 = ['Roosevelt', 'F', 'D']
        author4 = ['Tolkien', 'J', 'R', 'R']
        author1_fixed = _syntactic_parser(author1)
        author2_fixed = _syntactic_parser(author2)
        author3_fixed = _syntactic_parser(author3)
        author4_fixed = _syntactic_parser(author4)
        self.assertEqual(['J', 'P', 'Morgan'], author1_fixed)
        self.assertEqual(['Alfred', 'Nobel'], author2_fixed)
        self.assertEqual(['F', 'D', 'Roosevelt'], author3_fixed)
        self.assertEqual(['J', 'R', 'R', 'Tolkien'], author4_fixed)


if __name__ == '__main__':
    unittest.main()
