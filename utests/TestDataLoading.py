import os
import unittest

from data_parser.data_parser import import_data_from_cvs

path_to_cvs = os.path.join(os.getcwd(), 'utests/Resources/publications_min.csv')


class TestDataLoading(unittest.TestCase):

    @unittest.skipIf(not os.path.exists(path_to_cvs), 'cvs file doesn\'t exist in provided path')
    def test_loading_cvs(self):
        self.assertIsNotNone(import_data_from_cvs(path_to_cvs))

    def test_loading_nonexistent_cvs(self):
        self.assertIsNone(import_data_from_cvs('nonexistent_path.cvs'))

    @unittest.skipIf(not os.path.exists(path_to_cvs), 'cvs file doesn\'t exist in provided path')
    def test_loading_single_column_by_index(self):
        data = import_data_from_cvs(cvs_file=path_to_cvs, column_indices=[5])
        self.assertEqual(1, data.shape[1])

    @unittest.skipIf(not os.path.exists(path_to_cvs), 'cvs file doesn\'t exist in provided path')
    def test_loading_single_column_by_name(self):
        data = import_data_from_cvs(cvs_file=path_to_cvs, column_headers=['abstract'])
        self.assertEqual(1, data.shape[1])


if __name__ == '__main__':
    unittest.main()
