import logging
import re
import time
from os import PathLike
from typing import Optional, List

import pandas as pd

from data_models.Author import Author


def import_data_from_cvs(cvs_file: PathLike,
                         column_indices: List[int] = None,
                         column_headers: List[str] = None) -> Optional[pd.DataFrame]:
    """
    Load data (whole file or just a column selected by index/header) from a CVS file as a pandas DataFrame.
    :param cvs_file: path to a cvs file or a buffer
    :param column_indices: list of indices of columns to import
    :param column_headers: list of names (headers) of columns to import
    :return: data from CVS in a DataFrame or None in case of error/missing data
    """
    _params = []
    if column_indices:
        _params.extend(column_indices)
    if column_headers:
        _params.extend(column_headers)
    if not _params:
        _params = None
    try:
        with_nans = pd.read_csv(filepath_or_buffer=cvs_file,
                                usecols=_params)
        return with_nans.dropna()
    # Handles FileNotFoundError, PermissionError and other I/O types of errors
    except OSError:
        return None


def _get_series_of_all_authors_occurrences(authors_dataframe: pd.DataFrame) -> pd.Series:
    """
    Transitive function, to convert input DataFrame of size (1, XXXX)
    where each entry contains a list of authors to a pandas Series with each entry containing only 1 author.
    This is a raw conversion, duplicate  entries may occur at this point.
    :return: a list (pd.Series) of authors
    """
    # In the df lists of authors are actually string representation of python lists,
    # so lets convert them to actual python lists
    # remove double spaces, tabs, other white chars and replace with space
    # df_authors_lists = authors_dataframe.authors.str.strip.
    df_authors_lists = authors_dataframe.authors.str.strip('][').str.split(', ')
    # if we would like to avoid hardcoded column name authors we can pass index of the column in parameter and:
    # df_authors_lists = authors_dataframe.iloc[:,0].str.strip('][').str.split(', ')
    series_authors_lists = df_authors_lists.squeeze()
    return series_authors_lists.explode(ignore_index=True)


def _convert_author_string_to_list(_author_as_str: str) -> List[str]:
    """
    Convert author as string of format to a list
    eg. "J. D. Salinger" -> ["J", "D", "Salinger"]
    :param author_as_str: author's names and surname as a string
    :return: a list containing each name/initial and surname as a separate string
    """
    # remove possible '.' after initials
    author_as_str = _author_as_str
    author_as_str = author_as_str.replace('.', '')
    author_as_str = author_as_str.strip('\'')
    author_as_str = author_as_str.strip('\"')
    author_as_list = author_as_str.split(sep=' ')
    # Merge ['Leonardo', 'da', 'Vinci'] back into ['Leonardo', 'da Vinci']
    for index, element in enumerate(author_as_list):
        if element.lower() in ['von', 'van', 'da', 'de', 'di', 'av', 'la'] and index + 1 < len(author_as_list):
            author_as_list[index] += ' ' + author_as_list[index + 1]

    return [element for element in author_as_list if
            element]  # getting rid of empty strings which come up in case of names containing double spaces


def _syntactic_parser(author_as_list: List[str]) -> List[str]:
    """
    Match various name notations and convert to one: [name1, name2, ..., surname]
    :param author_as_list: A list containing all author's names and surname as separate strings
    :return: A list where author's names are at the beginning (index 0...n-2) and surname is last (index n-1)
    """
    # move initials before surname if they are after: ["Rowling", "J", "K"] -> ["J", "K", "Rowling"]
    # only if all names after surname are initials so that we don't mess up Franklin D Roosevelt into D Roosevelt Franklin
    surname, *names = author_as_list  # *names stores the rest of elements in a list left after unpacking surname
    surname_on_first_place = True if len(surname) > 1 else False
    names_are_initials = True
    for name in names:
        if len(name) > 1:
            names_are_initials = False
    if surname_on_first_place and names_are_initials:
        # rewrite a list of names and surname in a correct order
        author_as_list = names
        author_as_list.append(surname)

    return author_as_list


def parse_authors_from_cvs(cvs_file: PathLike) -> pd.Series:
    """
    Wrapper function taking in cvs file with authors and returning them in a pandas series of Author objects
    :param cvs_file: a path to CVS file
    :return: a pandas series with authors as lists of strings [name1, name2, surname]
    """
    start = time.time()
    df_authors_from_cvs_raw = import_data_from_cvs(cvs_file=cvs_file, column_headers=['authors'])
    end = time.time()
    logging.debug(f'(1/5)--import_data_from_cvs--loaded data from CSV file in {end - start} s.')
    start = time.time()
    authors_singled_out_in_series = _get_series_of_all_authors_occurrences(authors_dataframe=df_authors_from_cvs_raw)
    end = time.time()
    logging.debug(
        f'(2/5)--_get_series_of_all_authors_occurrences--split authors into separate strings and squeezed df into a series in {end - start} s.')
    start = time.time()
    authors_singled_out_in_series = authors_singled_out_in_series.apply(func=_convert_author_string_to_list)
    end = time.time()
    logging.debug(f'(3/5)--_convert_author_string_to_list--split authors names strings into lists {end - start} s.')
    start = time.time()
    authors_singled_out_in_series = authors_singled_out_in_series.apply(func=_syntactic_parser)
    end = time.time()
    logging.debug(
        f'(4/5)--_syntactic_parser--unified name schema for all authors. Names first, surname last in {end - start} s.')
    start = time.time()
    authors_singled_out_in_series = authors_singled_out_in_series.apply(func=Author)
    end = time.time()
    logging.debug(
        f'(5/5)--Author.__init__--created Author objects from lists of names and surnames in {end - start} s.')
    return authors_singled_out_in_series


def _find_all_emails(text: str) -> List[str]:
    """
    Find all emails in a string
    :param text: a string to look for emails in
    :return: a list containing all matches (emails)
    """
    return re.findall(r" ([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)", text)


def _remove_emails(_text: str) -> str:
    """
    Remove emails from text
    :param _text: source text to remove emails from
    :return: copy of the source text with emails removed
    """
    text = _text
    emails = _find_all_emails(text)
    for email in emails:
        text = text.replace(email, '')
        text = text.replace('email:', '')
    return text


def _single_out_institutions_from_str(institutions_bundled: str) -> List[str]:
    """
    Convert string containing multiple institutions into list of strings containing single institutions
    :return: a list of institutions
    """
    # In the series lists of institutions are actually string representation of python lists,
    # so lets convert them to actual python lists
    institutions_bundled = institutions_bundled.strip('][')
    institutions_bundled = institutions_bundled.replace(';', '., ')
    institutions_bundled = institutions_bundled.strip()
    institutions_list = institutions_bundled.split('., ')
    institutions_list = [institution.strip('\'') for institution in institutions_list]
    institutions_list = [institution.strip('\"') for institution in institutions_list]
    # clean up empty elements such as ", , , , , , , , "
    for index, element in enumerate(institutions_list):
        # institutions_list[index] = institutions_list[index].strip('.')
        if not re.search('[a-zA-Z]', element):
            institutions_list.pop(index)
    return institutions_list


def parse_institutions_from_cvs(cvs_file: PathLike) -> pd.Series:
    """
    Wrapper function taking in cvs file with institutions (affiliations)
    and returning them in a pandas series of Institution objects
    :param cvs_file: a path to CVS file
    :return: a pandas series with institutions as lists of strings [name of the institution, [city], [country]]
    """
    start = time.time()
    df_institutions_from_cvs_raw = import_data_from_cvs(cvs_file=cvs_file, column_headers=['affiliations'])
    end = time.time()
    logging.debug(f'(1/3)--import_data_from_cvs--loaded data from CSV file in {end - start} s.')
    series_institutions_raw = df_institutions_from_cvs_raw.squeeze()
    start = time.time()
    series_institutions = series_institutions_raw.apply(func=_remove_emails)
    end = time.time()
    logging.debug(f'(2/3)--_remove_emails--got rid of emails from the institutions names in {end - start} s.')
    start = time.time()
    series_institutions = series_institutions.apply(func=_single_out_institutions_from_str)
    end = time.time()
    logging.debug(
        f'(3/3)--_single_out_institutions_from_str--split each institution into a separate element of the list in {end - start} s.')
    series_institutions = series_institutions.explode(ignore_index=True)
    series_institutions = series_institutions.str.replace(r'^\W+|\W+$', '', regex=True)
    series_institutions = series_institutions[series_institutions.str.contains("[a-zA-Z]").fillna(False)]
    series_institutions.dropna(inplace=True)
    return series_institutions
