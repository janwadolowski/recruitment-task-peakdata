import logging
import time
from typing import List

import pandas as pd

from data_models import Author


def filter_unique_authors(authors: pd.Series) -> List[Author]:
    start = time.time()
    authors.drop_duplicates(inplace=True)  # duplicates are evaluated based on __hash__() method
    end = time.time()
    logging.debug(
        f'(1/2)--drop_duplicates--removed all duplicates from the series based on hashing initials + surname in {end - start} s.')
    start = time.time()
    authors_list = authors.tolist()
    end = time.time()
    logging.debug(
        f'(2/2)--tolist--converted pd.Series to a list in {end - start} s.')
    return authors_list


def filter_unique_institutions(institutions: pd.Series) -> List[Author]:
    start = time.time()
    # I assume we treat different departments of an institution as different institutions
    institutions.drop_duplicates(inplace=True)
    end = time.time()
    logging.debug(
        f'(1/2)--drop_duplicates--removed all duplicates from the series in {end - start} s.')
    start = time.time()
    institutions_list = institutions.tolist()
    end = time.time()
    logging.debug(
        f'(2/2)--tolist--converted pd.Series to a list in {end - start} s.')
    return institutions_list
