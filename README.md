# Given a list of medical publications, provide a list of unique authors and institutions

For purposes of PeakData recruitment process

Author of the solution: Jan Wądołowski

[[_TOC_]]

## Setup And Usage

### Prerequisites

* Python3.9.7 installed (should work on 3.7+ as well but haven't tested)
* Pip3 installed
* Tested on Linux (Ubuntu), but should work on other distros and systems (still, use UNIX paths in CLI)

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/janwadolowski/recruitment-task-peakdata.git
   ```
2. cd into it (`cd ./recruitment-task-peakdata`)
3. Create a virtual environment and activate it
   ```sh
   python3 -m venv venv
   source ./venv/bin/activate
   ```
4. Install required python modules
   ```sh
   pip install -r requirements.txt
   ```
Your environment is set up, and you can run the application

### Usage

* if you want to receive a list of unique authors run `python3 -m main /path/to/csv_file -a`. It will display the list to the terminal.
* if you want to receive a list of unique institutions run `python3 -m main /path/to/csv_file -i`
* for either of the options you can specify parameter `-o` to write the output to a text file. Like so:
```sh
python3 -m main /path/to/csv_file -a  -o  /path/to/save/text_file
```
* you can display help menu with: `python3 -m main -h`

## My Approach to the Task

### Authors
* I assumed  that if initials and surname of one person match the number and order of names/initials and surname of the other person, then it's the same person (so J D Brown == Joseph Douglas Brown, Jim Daniel Brown etc. they are all duplicates, so I only keep 1 name)
* I observed that authors are stored in a list-like string in the column 'authors', so I loaded that column as pandas DataFrame and converted the strings in each row accordingly to have a dataframe with lists of authors.
* Then I split again each author string on space (' ') to have each author's name/surname as a separate element of a list and each author as a separate list.
* I created a model (dataclass) to store data about a single author. I needed a custom object to implement comparison method between the authors, due to non-standard way of comparing strings (as described in 1.)
* Due to varying number of names per author I construct an Author object by list unpacking like so:
```python
 def __init__(self, author_as_list: List[str]):
        *self.names, self.surname = author_as_list
```
* I also took care of possibility that initials come after surname (Tolkien J R R) or that there is a full stop after initials (J. .K Rowling) 
* To get rid of duplicates I used the library function `pandas.Series.drop_duplicates()` which utilises a `__hash__()` method of the object stored in data series row, which I implemented for Author
* My hash method implementation:
```python
  def __hash__(self):
        hashable_name = [name[0] for name in self.names]  # for comparison purposes we're hashing only initials
        hashable_name.append(self.surname)
        hash(' '.join(hashable_name)) # If author is called 'John Richard Smith' we hash 'J R Smith' 
```
* Unfortunately it turned out that although my solution work as expected it is rather slow. For a given data sample it needs about 500 seconds on a 8GB/Intel i5 VM to finish the job.

### Institutions
* As for institutions, I assumed that each branch of an institution is a separate institution.
* I loaded CSV file into a DataFrame analogically to authors  
* I noticed that institutions are in a list (1 list per publication) and each institution is separated by '., ' characters sequence or by ';'
* So I converted the data so that I had a Series containing only one institution per row
* To filter out unique institutions I again used `pandas.Series.drop_duplicates()`, however, this time objects stored in a series were strings (as I compared each institution 'as is' in a CSV) so I didn't need to introduce a new data type or define a hashing method.
* dropping duplicates for institutions was very fast.

## Potential Failure Points and Bottlenecks

* Dropping duplicates of authors is very slow even with small dataset (10_000). With larger sets it's not efficient enough.
* pandas Dataframe and Series are RAM-bound so in case of larger collection this solution will fail or be unstable
* loading huge data from a file is generally a bad idea performance wise, so in case of scaling up a database would be a better solution. If not then splitting a large collection into several CSV files and introducing a batch loader to the program
* if schema in CSV file changes the program will fail (column headers 'authors' and 'affiliations' are hardcoded) 

## What Is Needed Before Deployment to Production

* much more tests to tackle corner cases.
* identification of possible memory leaks / choking point of `pandas.Series.drop_duplicates()` for a Series with Authors
* some more exception handling. I only handled the most basic exceptions.
* 'foolproofing' user input. There are probably dozens of scenarios how a user voluntarily or by accident can cause program to malfunction by passing weird arguments to `main()`

## Contact

It was a really fun project to develop and way more challenging than I expected. I would love to hear feedback from you guys about my solution regardless of outcome of my recruitment. :)

Jan Wądołowski - [linkedin.com/in/jan-wadolowski/](https://www.linkedin.com/in/jan-wadolowski/)

Project
Link: [gitlab.com/janwadolowski/recruitment-task-peakdata](https://gitlab.com/janwadolowski/recruitment-task-peakdata)
