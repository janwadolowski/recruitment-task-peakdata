import argparse
import logging
import os
import pprint
from pathlib import Path

import data_parser

logging.basicConfig(level=logging.DEBUG, filename='app.log', filemode='w')


def main() -> None:
    # ---- CLI
    parser = argparse.ArgumentParser()
    parser.add_argument("input", help="path to CVS file to load data from. Use UNIX path notation.")
    parser.add_argument("-o", "--output", help="path to txt file to save data in. Use UNIX path notation.")
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("-a", "--authors", action="store_true", help="flag to select unique authors")
    group.add_argument("-i", "--institutions", action="store_true", help="flag to select unique institutions")
    args = parser.parse_args()
    # ----
    if args.authors:
        logging.debug(f'@@@ DATA PARSER')
        loaded_cvs_data = data_parser.parse_authors_from_cvs(cvs_file=args.input)
        logging.debug(f'@@@ DATA FILTER')
        list_of_uniques = data_parser.filter_unique_authors(authors=loaded_cvs_data)
    else:
        logging.debug(f'@@@ DATA PARSER')
        loaded_cvs_data = data_parser.parse_institutions_from_cvs(cvs_file=args.input)
        logging.debug(f'@@@ DATA FILTER')
        list_of_uniques = data_parser.filter_unique_institutions(institutions=loaded_cvs_data)
    if args.output:
        output_file = Path(args.output)
        with open(output_file, 'wt') as file:
            pprint.pprint(list_of_uniques, file, width=99999)
    pprint.pprint(list_of_uniques, width=99999)
    print('All done.', end='')
    if args.output:
        print(f' If you selected option to output data to file (-o, --output) you will find this list in {output_file}')
    else:
        print('')  # add newline


if __name__ == '__main__':
    main()
