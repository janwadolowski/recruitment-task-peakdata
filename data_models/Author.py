from dataclasses import dataclass
from typing import List


@dataclass()
class Author:
    names: List[str]
    surname: str

    def __init__(self, author_as_list: List[str]):
        *self.names, self.surname = author_as_list

    # My way of comparing is to always compare all names initials and a full surname (logical conjunction).
    # If author has many names and is written in full names and in initials all his names must have initials
    # eg. John Ronald Reuel Tolkien == J R R Tolkien, but John Ronald Reuel Tolkien != J Tolkien
    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            raise NotImplementedError
        else:
            # Compare surname
            if self.surname != other.surname:
                return False
            # If one author has more names than the other they are not the same
            if len(self.names) != len(other.names):
                return False
            # Compare all names
            for self_name, other_name in zip(self.names, other.names):
                # If one of the names is an initial only compare first char in both names
                if len(self_name) == 1 or len(other_name) == 1:
                    if self_name[0] != other_name[0]:
                        return False
                # Else compare full names
                else:
                    if self_name != other_name:
                        return False
            # If reached here all the names and the surnames must be equal according to the vetting algorithm
            return True

    def __hash__(self):
        hashable_name = [name[0] for name in self.names]  # for comparison purposes we're hashing only initials
        hashable_name.append(self.surname)
        hash(' '.join(hashable_name))

    def __str__(self):
        output = ' '.join(self.names)
        output += ' ' + self.surname
        return output

    def __repr__(self):
        output = ' '.join(self.names)
        output += ' ' + self.surname
        return output
